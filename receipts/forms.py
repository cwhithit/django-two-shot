from django.forms import ModelForm
from receipts.models import ExpenseCategory, Receipt, Account

class ReceiptForm(ModelForm):
    class Meta:
        model= Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class AccountForm(ModelForm):
    class Meta:
        model=Account
        fields =[
            "name",
            "number",
        ]

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields =[
            "name",
        ]
